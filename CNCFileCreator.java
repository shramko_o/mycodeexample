package Main;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.awt.geom.PathIterator;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.Locale;
import Main.ProductionInfo.CuttingTool;

public class CNCFileCreator {

    private ObservableList<PartSize> partData = FXCollections.observableArrayList();





    public CNCFileCreator(String dest, ArrayList<PartSize> partData, ProductionInfo pi, String arrangeType) throws IOException {


        System.out.println("Try create"+partData+pi);
        String myPathToFile;

        for (PartSize ps:partData
             ) {

            //Create part-named folder
            String partTypeName = pi.getNumber();

            File jarFile = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            //File file = new File(jarFile.getParentFile().getParent(), "files/file.txt");
            String pathname=jarFile.getParentFile().getPath();
            System.out.println(pathname);

            myPathToFile = pathname+File.separator+"УП" +File.separator+dest+File.separator +partTypeName+" "+ps.getHeight()+"x"+ps.getWidth();
            File file = new File(myPathToFile);
            file.mkdirs();

            Runtime.getRuntime().exec("chmod -R 777 "+pathname+File.separator+"УП" +File.separator+dest);
System.out.println("chmod for " + pathname+File.separator+"УП" +File.separator+dest);
            int i=1;
            for (CuttingTool t: pi.getTools()){

                file = new File(myPathToFile+File.separator+Integer.toString(i)+" "+t.getToolName()+ ".cnc");

                try
                {
                    StringBuilder sb = new StringBuilder();
                    BufferedWriter bw = new BufferedWriter(new FileWriter(file));
                    PathWriter pw = new PathWriter(bw,sb,ps,t.getPaths());
                    bw.close();
                    String filePathString = myPathToFile+File.separator+Integer.toString(i)+" "+t.getToolName()+ ".cnc";
                    CNCFileLengthCalculator lengthCalculator = new CNCFileLengthCalculator(filePathString);
                    CNCToolpathLength length = lengthCalculator.Calculate();


                    System.out.println(filePathString + "\t GO = " + length.getG0Length()+ "\t G1 = " + length.getG1Length() +"\r\n" );


                    i++;
                } catch (IOException e)
                {
                    e.printStackTrace();
                    System.out.println(e);
                }
            }


        }






    }


}