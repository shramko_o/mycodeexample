package Main;
import Main.ProductionInfo.PathInfo;
import java.io.*;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import Main.ShapeWriters.*;

public class PathWriter {
    public PathWriter(BufferedWriter bw, StringBuilder svg, PartSize pSize, List<PathInfo> cuttingToolPaths) {

        File file;
        FileInputStream fi;

        String path="C:\\Users\\Administrator\\IdeaProjects\\CNCToolPathsMaker\\PartTypes\\"; //to dxf files
        try {

            bw.write("%\r\nG90\r\nG49\r\nM3 S15000\r\n");
            for(PathInfo pathInfo : cuttingToolPaths) {
                switch (pathInfo.shape) {
                    case "rect":
                        new RectWriter(bw,svg,pSize, pathInfo);
                        break;
                    case "rectclear":
                        new RectClear(bw,pSize, pathInfo);
                        break;
                    case "vsharprect":
                        new VSharpRectWriter(bw,pSize, pathInfo);
                        break;

                    case "toparcrect":
                        new TopArcedRect(bw,pSize, pathInfo);
                        break;
                    case "vsharptoparcrect":
                        new VSharpedTopArcedRect(bw,pSize, pathInfo);
                        break;
                    case "vsharptoparcedwithoffset":
                        new VSharpTopOffsArcRect(bw,pSize, pathInfo);
                        break;

                    case "vlines":
                        new VerticalLinesWriter(bw,pSize, pathInfo);
                        break;
                    case "outervlines":
                        new OuterVerticalLinesWriter(bw,pSize, pathInfo);
                        break;
                    case "offsetdrill":
                        new OffsetDrill(bw,pSize, pathInfo);
                        break;
                    case "cornerdrill":
                        new CornerDrill(bw,pSize, pathInfo);
                        break;
                    case "dxf":
                        new DXF(bw,new DXFE().extract(path+pathInfo.getParameters().get(0)),pSize, pathInfo);
                        break;
                    case "ssdxf":
                        new SmartStretchDXF(bw,new DXFE().extract(path+pathInfo.getParameters().get(0)),pSize, pathInfo);
                        break;
                    case "insidexsdxf":
                        new InsideXStretchedDXF(bw,new DXFE().extract(path+pathInfo.getParameters().get(0)),pSize, pathInfo);
                        break;
                    case "dxfour":
                        new DXFour(bw,new DXFE().extract(path+pathInfo.getParameters().get(0)),pSize, pathInfo);
                        break;
                    case "xstertcheddxf":
                        new XStretchedTwoDXF(bw,new DXFE().extract(path+pathInfo.getParameters().get(0)),pSize, pathInfo);
                        break;
                    case "centeredtwodxf":
                        new CenteredTwoDXF(bw,new DXFE().extract(path+pathInfo.getParameters().get(0)),pSize, pathInfo);
                        break;
                    case "dxfalongrect":
                        new DXFAlongRect(bw,new DXFE().extract(path+pathInfo.getParameters().get(0)),pSize, pathInfo);
                        break;
                    case "stretcheddxfalongrect":
                        new StretchedDXFAlongRect(bw,new DXFE().extract(path+pathInfo.getParameters().get(0)),pSize, pathInfo);
                        break;

                    case "vrsharp":
                        new VRSharp(bw,new DXFE().extract(path+pathInfo.getParameters().get(0)),pSize, pathInfo);
                        break;
                    case "tile":
                        new Tile(bw,svg,new DXFE().extract(path+pathInfo.getParameters().get(0)),pSize, pathInfo);
                        break;

                }
            }
            bw.write("G0 Z15\r\nG0 X0.000 Y0.000\r\nM05\r\nM02");
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }
    private void shapeWriter(Double offset, PartSize partSize) {
        String x1 = String.format(Locale.ENGLISH, "%1$.3f ", offset);
        String x2 = String.format(Locale.ENGLISH, "%1$.3f ", Double.parseDouble(partSize.getWidth()) - offset);
        String y1 = String.format(Locale.ENGLISH, "%1$.3f ", offset);
        String y2 = String.format(Locale.ENGLISH, "%1$.3f ", Double.parseDouble(partSize.getHeight()));
    }

}
