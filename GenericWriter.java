package Main.ShapeWriters;

import Main.PartSize;
import Main.ProductionInfo.PathInfo;
import org.kabeja.dxf.helpers.Point;

import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class GenericWriter {
    

    double xOffset;
    double yOffset;
    List<Double> depth = new ArrayList<Double>();
    double h;
    double w;
    double fixedH;
    double fixedW;

    List<String> parameters;
    double xStart;
    double yStart;
    double rotation;

    public void getPathInfo(PartSize ps,PathInfo pi){
        xOffset = sToD(pi.getxOffset()) + sToD(ps.getGXOffset());
        yOffset = sToD(pi.getyOffset()) + sToD(ps.getGYOffset());

        parameters = pi.getParameters();

        fixedH = getParameterValue("fixedH",0.0);
        fixedW = getParameterValue("fixedW",0.0);

        if(fixedH<0.001) h = sToD(ps.getHeight()); else h = fixedH;
        if(fixedW<0.001) w = sToD(ps.getWidth()); else w = fixedW;

        for (String d : pi.getDepth()){
            depth.add(sToD(d));
        }

        xStart = getParameterValue("xStart",0.0);
        yStart = getParameterValue("yStart",0.0);
        rotation = getParameterValue("rotation",0.0);

    }
    
    public String dToS(double d){
        return String.format(Locale.ENGLISH,"%.3f",d);

    }
    double sToD(String s){
        return Double.parseDouble(s);
    }

    boolean parametersContain(String s){
        for (String p : parameters){
            if (p.compareTo(s)==0) return true;
        }
        return false;
    }
    int  parameterNumber(String s){
        if (parameters.size()<1)return -1;
        for (int i=0;i<parameters.size();i++){
            //parameters.get(i).contains(s);
            //if (parameters.get(i).compareTo(s)==0) return i;
            if (parameters.get(i).contains(s)) return i;

        }
        return -1;
    }

    double getParameterValue(String parameterName,double defaultValue){
        double result = defaultValue;
        int n = parameterNumber(parameterName);
        if (n==-1) return result;
        parameters.get(n).compareTo("=");
        String[] parts = parameters.get(n).split(" = ");
        System.out.println(parts[1].trim()); // STRING_VALUES
        result = sToD(parts[1]);
        return result;
    }

    Point rotatePoint(Point kbjpt, double angle)
    {
        //double mangle = ((angle/180)*Math.PI);
        //double cosAngle = Math.cos(mangle);
        //double sinAngle = Math.sin(mangle);
        double[] pt = {kbjpt.getX(), kbjpt.getY()};

        AffineTransform.getRotateInstance(Math.toRadians(angle), 0.0, 0.0)
                .transform(pt, 0, pt, 0, 1); // specifying to use this double[] to hold coords
        //double newX = pt[0];
        //double newY = pt[1];

        //pt.setX(center.x + (int) ((pt.x-center.x)*cosAngle-(pt.y-center.y)*sinAngle));
        //pt.setY(center.y + (int) ((pt.x-center.x)*sinAngle+(pt.y-center.y)*cosAngle);
        //pt.setX( pt.getX()*cosAngle-pt.getY()*sinAngle);
        //pt.setY( pt.getX()*sinAngle+pt.getY()*cosAngle);

        return new Point(pt[0],pt[1],kbjpt.getZ());
    }
}
