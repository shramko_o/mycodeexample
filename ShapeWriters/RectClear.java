package Main.ShapeWriters;

import Main.Main;
import Main.PartSize;
import Main.ProductionInfo.PathInfo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RectClear extends GenericWriter{
    public RectClear(BufferedWriter bw, PartSize pSize, PathInfo pi) {
        try {

            getPathInfo(pSize,pi);

            double D = getParameterValue("D",100.0); // Tool diameter
            double step = getParameterValue("STEP_IN_PERCENT_OF_D",80.0) / 100; // StepOver Coefficient got in %



            double x1 = (xStart +  xOffset + D/2);
            double x2 = (xStart +  w - xOffset - D/2);
            double y1 = (yStart +  yOffset + D/2);
            double y2 = (yStart +  h - yOffset - D/2);





            for (int i = 0; i < depth.size(); i++) {

                bw.write("G0 Z15.000\r\n X" + x1 + " Y" + y1 + "\r\nG0 Z1.000\r\n");
                bw.write("G1\tZ-" + dToS(depth.get(i)) + "\tF600\r\n"); //Move down to next depth

                boolean isGoingRight=true;
                double currentY = y1;
                do {

                    if (isGoingRight) bw.write("X" + dToS(x2) + "\tF2000\r\n");
                    else bw.write("X" + dToS(x1) + "\tF2000\r\n");
                    currentY += D*step;
                    isGoingRight = !isGoingRight;
                    bw.write("Y" + dToS(currentY) + "\r\n");

                    //if(currentY > y2 - D) if (currentY < y2) currentY = y2;

                }while (currentY < y2 - D*step);
                if (isGoingRight) bw.write("X" + dToS(x2) + "\tF2000\r\n");
                else bw.write("X" + dToS(x1) + "\tF2000\r\n");

                currentY = y2;
                isGoingRight = !isGoingRight;
                bw.write("Y" + dToS(currentY) + "\r\n");

                if (isGoingRight) bw.write("X" + dToS(x2) + "\tF2000\r\n");
                else bw.write("X" + dToS(x1) + "\tF2000\r\n");

                //bw.write(String.format(Locale.ENGLISH, "X%1$.3f ", path.getParameters().get(1)));
            }
            bw.write("G0 Z15.000\r\n");

            bw.write("X" + dToS(x2) + " Y" + dToS(y1) + "\r\nZ1.000\r\n");

            bw.write("G1 Z" + dToS(-depth.get(depth.size()-1))+ "\r\n");

            bw.write("X" + x2 + "Y" + y1 + "\tF2000\r\n");
            bw.write("X" + x2 + "Y" + y2 + "\r\n");
            bw.write("X" + x1 + "Y" + y2 + "\r\n");
            bw.write("X" + x1 + "Y" + y1 + "\r\n");


        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }
}
