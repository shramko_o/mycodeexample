package Main.ShapeWriters;

import Main.Main;
import Main.PartSize;
import Main.ProductionInfo.PathInfo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

//This class is to make four drill holes in the centers of offseted rectangle lines
public class CornerDrill extends GenericWriter {
    public CornerDrill(BufferedWriter bw, PartSize pSize, PathInfo pi) {
        getPathInfo(pSize, pi);
        try {

            String x1 = dToS(xStart + xOffset);
            String x2 = dToS(xStart + w - xOffset);


            String y1 = dToS(yStart + yOffset);
            String y2 = dToS(yStart + h - yOffset);

            WriteDrillPath(bw,x1,y1,6,20);
            WriteDrillPath(bw,x2,y1,6,20);
            WriteDrillPath(bw,x2,y2,6,20);
            WriteDrillPath(bw,x1,y2,6,20);

            bw.write("G0 Z15.000\r\n");
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    private void WriteDrillPath(BufferedWriter bw, String X, String Y, double startDepth, double depth) {

        try {

            bw.write("G0 X" + X + "\tY" + Y  + "\r\n");
            bw.write("G0 Z" + dToS(-startDepth+1) + "\r\n");
            bw.write("G1 Z" + dToS(-depth) + "\r\n");
            bw.write("G0 Z15.000\r\n");


        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

    }
}
