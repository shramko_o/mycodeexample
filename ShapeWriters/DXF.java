package Main.ShapeWriters;

import Main.Main;
import Main.PartSize;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import Main.ProductionInfo.PathInfo;

import org.kabeja.dxf.DXFPolyline;
import org.kabeja.dxf.DXFVertex;

public class DXF extends GenericWriter{


    public DXF(BufferedWriter bw, List<DXFPolyline> plines, PartSize pSize, PathInfo pi) {
        try {

            getPathInfo(pSize,pi);

            int needXMirror =0;
            if (xOffset>0.000)needXMirror=1;


            DXFVertex vertex;

            bw.write("G0 Z15.000");

            for (int xMultiplier = 0;xMultiplier<=needXMirror;xMultiplier++) {



                for (DXFPolyline pline:plines) {

                        vertex = pline.getVertex(0);
                        bw.write("G0 X" + dToS( (1-xMultiplier)*(xOffset+vertex.getX())+xMultiplier*(w-xOffset-vertex.getX()))
                             + "\tY" + dToS( vertex.getY()+yOffset) + "\r\nZ1.000\r\n");
                        bw.write("G1\tZ-" + depth.get(0) + "\tF300\r\n");
                        for (int i = 1; i < pline.getVertexCount(); i++) {
                            vertex = pline.getVertex(i);
                            bw.write("X" + dToS( (1-xMultiplier)*(xOffset+vertex.getX())+xMultiplier*(w-xOffset-vertex.getX()))
                            + "\tY" + dToS( vertex.getY()+yOffset) + "\r\n");
                         }
                        bw.write("G0 Z15.000\r\n");
                    }

            }
            bw.write("G0 Z15.000\r\n");

        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName())
                    .log(Level.SEVERE, null, ex);
        }


    }






}
