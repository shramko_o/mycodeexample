package Main.ShapeWriters;

import Main.Main;
import Main.PartSize;
import Main.ProductionInfo.PathInfo;
import java.awt.geom.Arc2D;
import java.awt.geom.PathIterator;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TopArcedRect extends GenericWriter{
    public TopArcedRect(BufferedWriter bw, PartSize pSize, PathInfo pi) {
        try {


            Double arcH = sToD(pi.getParameters().get(0));       //height of arc

            //r=arcH/2+c^2/8h
            Double c=w-xOffset*2;

            Double r = arcH/2+(c*c)/(8*arcH);
            // cos⁡〖α/2〗=1-arcH/r
            Double angle = Math.toDegrees(2*Math.acos(1-arcH/r));
            String x1 = dToS( xOffset);
            String x2 = dToS( w - xOffset);
            String y1 = dToS( yOffset);
            String y2 = dToS( h - yOffset);

            Arc2D.Double  rr = new Arc2D.Double(w/2-r,h-xOffset-r*2,r*2,r*2,angle/2-90,-angle,Arc2D.OPEN);

            bw.write("G0 Z15.000\r\n");
            bw.write("G0 X" + x1 + " Y" + y1 + "Z15.000\r\nZ1.000\r\n");
            for (int i = 0; i < depth.size(); i++) {
                bw.write("G1\tZ-" + depth.get(i) + "\tF300\r\n"); //Move down to next depth
                bw.write("X" + x2 + "\tF2000\r\n");

                double[] coords = new double[6];
                for (PathIterator pit = rr.getPathIterator(null,0.02); !pit.isDone(); pit.next()) {
                    // The type will be SEG_LINETO, SEG_MOVETO, or SEG_CLOSE
                    // Because the Area is composed of straight lines
                    int type = pit.currentSegment(coords);
                    bw.write(String.format(Locale.ENGLISH,"X%1$.3f ", coords[0])+String.format(Locale.ENGLISH,"Y%1$.3f \r\n", coords[1]));

                }

                bw.write("X" + x1 + "\r\n");
                bw.write("Y" + y1 + "\r\n");


                //bw.write(String.format(Locale.ENGLISH, "X%1$.3f ", path.getParameters().get(1)));
            }
            bw.write("G0 Z15.000\r\n");
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }
}
