package Main.ShapeWriters;

import Main.Main;
import Main.PartSize;
import Main.ProductionInfo.PathInfo;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RectWriter extends GenericWriter{
    public RectWriter(BufferedWriter bw,StringBuilder svg, PartSize pSize, PathInfo pi) {
        try {

            getPathInfo(pSize,pi);


            String x1 = dToS(xStart +  xOffset);
            String x2 = dToS(xStart +  w - xOffset);
            String y1 = dToS(yStart +  yOffset);
            String y2 = dToS(yStart + h - yOffset);

            String rectW = dToS(  xStart + w - xOffset*2);
            String rectY = dToS(  yStart + h - yOffset*2);


            //svg.append("<rect x=\""+x1+"\" y=\""+y1+"\" rx=\"5\" ry=\"5\" width=\""+rectW+"\" height=\""+rectY+"\" style=\"fill:#aaaaaa;stroke:gray;stroke-width:5;opacity:0.5\" ></rect>"  );

            bw.write("G0 X" + x1 + " Y" + y1 + " Z15.000\r\nZ1.000\r\n");
            for (int i = 0; i < depth.size(); i++) {
                bw.write("G1\tZ-" + dToS(depth.get(i)) + "\tF300\r\n"); //Move down to next depth
                bw.write("X" + x2 + "\tF2000\r\n");
                bw.write("Y" + y2 + "\r\n");
                bw.write("X" + x1 + "\r\n");
                bw.write("Y" + y1 + "\r\n");


                //bw.write(String.format(Locale.ENGLISH, "X%1$.3f ", path.getParameters().get(1)));
            }
            bw.write("G0 Z15.000\r\n");
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }
}
